<?php
    require 'functions.php';
    $mahasiswa = nameQuery("SELECT * FROM mahasiswa");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Program Input Data Mahasiswa</title>
    <link rel="stylesheet" href="./style/style.css">
</head>
<body>
    <header>
        <nav>
            <a href="https://uad.ac.id/" target="_blank"><img src="./uad.jpg" alt="logo"></a>
            <ul>
                <li><a href="index.html">BERANDA</a></li>
                <li><a href="add.php">TAMBAH DATA</a></li>
                <li><a href="biodata.php">LIST BIODATA</a></li>
                <li><a href="feedback.php">FEEDBACK</a></li>
            </ul>
        </nav>
        <div class="header">
            <h1>PROGRAM INPUT DATA MAHASISWA</h1>
            <h2>UNIVERSITAS AHMAD DAHLAN</h2>
        </div>
    </header>
    <main>
        <article>
            <h1>BIODATA MAHASISWA</h1>
            <?php $i = 1; ?>
            <?php foreach($mahasiswa as $row) : ?>
            <table>
                <tr>
                    <td>NAMA</td>
                    <td>: <?= $row["nama"] ?></td>
                </tr>
                <tr>
                    <td>NIM</td>
                    <td>: <?= $row["nim"] ?></td>
                </tr>
                <tr>
                    <td>FAKULTAS</td>
                    <td>: <?= $row["fakultas"] ?></td>
                </tr>
                <tr>
                    <td>PRODI</td>
                    <td>: <?= $row["prodi"] ?></td>
                </tr>
            </table>
            <?php $i++; ?>
            <?php endforeach; ?>
        </article>
    </main>
    <footer>
        <p>&copy; 2023 - M Ikbal ALfa Rizi. 2200018352 - Universitas Ahmad Dahlan</p>
    </footer>
</body>
</html>
